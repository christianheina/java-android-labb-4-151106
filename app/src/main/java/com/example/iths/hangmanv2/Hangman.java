package com.example.iths.hangmanv2;

import java.util.ArrayList;
import java.util.Random;

/**
 * A class that handles all the logic behind a hangman game.
 *
 * Created by Christian Heina on 15-11-06.
 */
public class Hangman {
    private String theWord, badLetters;
    private ArrayList<String> allWords;
    private ArrayList<Character> usedLetters;
    private int tries;
    private boolean[] isVisible;

    /**
     * Creates a new Hangman game, and chooses a word.
     *
     * @param allWords - a list of all the words the game can pick.
     */
    public Hangman(ArrayList<String> allWords){
        this.allWords = allWords;
        newWord();
    }

    /**
     * Picks a word from the list and sets all the needed values for it.
     */
    public void newWord(){
        Random rand = new Random();
        theWord = allWords.get(rand.nextInt(allWords.size()-1));
        isVisible = new boolean[theWord.length()];
        tries = 10;
        usedLetters = new ArrayList<Character>();
        badLetters = null;
    }

    /**
     * Checks if the guessed letter is in the word. If the letter is in the word, the position will be set to true.
     * The letter will also be remembered as a used letter and if not in the word a bad letter.
     *
     * @param guess - the letter to check
     */
    public void guess(char guess){
        boolean badLetter = true;
        for(int i=0;i<isVisible.length;i++){
            if(guess == theWord.charAt(i)){
                isVisible[i] = true;
                badLetter = false;
            }
        }
        if(badLetter){
            addBadLetter(guess);
            tries--;
        }
        addUsedLetter(guess);
    }

    private void addUsedLetter(char usedLetter){
        usedLetters.add(usedLetter);
    }

    private void addBadLetter(char badChar){
        if(badLetters == null){
            badLetters = String.valueOf(badChar);
            return;
        }
        badLetters = badLetters + ", " + String.valueOf(badChar);
    }

    /**
     * @return the current word with all the letters not unlocked hidden.
     */
    public String getHiddenWord(){
        char[] temp = theWord.toCharArray();
        for(int i=0;i<isVisible.length;i++){
            if(!isVisible[i]){
                temp[i] = '-';
            }
        }
        return String.valueOf(temp);
    }

    /**
     * @return the word without any hidden letters.
     */
    public String getRealWord(){
        return theWord;
    }

    /**
     * @return the number of tries left
     */
    public int getTriesLeft(){
        return tries;
    }

    /**
     * @param c the char to check
     * @return true if the letter been used before else false
     */
    public boolean hasUsedLetter(char c){
        for(int i=0;i<usedLetters.size();i++){
            if(c == usedLetters.get(i)){
                return true;
            }
        }
        return false;
    }

    /**
     * @return a string containing all the letters used that wasn't in the word.
     */
    public String getBadLettersUsed(){
        return badLetters;
    }

    /**
     * @return true if all the letters in the word are visible.
     */
    public boolean hasWon(){
        for(int i=0;i<theWord.length();i++){
            if(!isVisible[i]){
                return false;
            }
        }
        return true;
    }

    /**
     * @return true if tries are equal or less than 0.
     */
    public boolean hasLost(){
        if(tries <= 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param c the char to check
     * @return true if the letter is between A to Z otherwise false.
     */
    public boolean isLetter(char c){
        if((c >= 'A' && c <= 'Z')){
            return true;
        }
        return false;
    }
}
