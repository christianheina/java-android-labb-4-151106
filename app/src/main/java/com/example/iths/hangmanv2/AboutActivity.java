package com.example.iths.hangmanv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/**
 * A class for the about activity.
 *
 * Created by Christian Heina on 15-11-06.
 */
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    /**
     * Adds a menu to the action bar.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    /**
     * when clicking the play icon or text in the actionbar this starts a new play activity and finishes this one.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.ic_play:
                Intent play = new Intent(this, PlayActivity.class);
                startActivity(play);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Starts a new MenuActivity and finishes this one.
     *
     * @param view - the onClick event from the user
     */
    public void aboutBackButton(View view){
        Intent back = new Intent(this, MenuActivity.class);
        startActivity(back);
        finish();
    }
}
