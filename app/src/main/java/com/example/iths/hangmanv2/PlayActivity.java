package com.example.iths.hangmanv2;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A class for the play activity.
 *
 * Created by Christian Heina on 15-11-06.
 */
public class PlayActivity extends AppCompatActivity {
    private EditText guess;
    private ImageView hangImage;
    private Hangman hangman;
    private TextView word, tries, guessed, notice;
    private boolean active;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        hangman = new Hangman(getAllWords());

        hangImage = (ImageView) findViewById(R.id.hangman_image);
        guess = (EditText) findViewById(R.id.guess);
        guessed = (TextView) findViewById(R.id.guessed);
        tries = (TextView) findViewById(R.id.tries);
        word = (TextView) findViewById(R.id.word);
        notice = (TextView) findViewById(R.id.notice);

        updateUI();
        notice.setVisibility(View.INVISIBLE);
    }

    /**
     * Adds a menu to the action bar.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    /**
     * when clicking the info icon or text in the actionbar this starts a new AboutActivity and finish() this one.
     * when clicking the play icon or text in the actionbar this picks a new word from hangman, updates UI and creates a message for the player.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.ic_play:
                hangman.newWord();
                updateUI();
                newNotice(getString(R.string.new_game));
                return true;
            case R.id.ic_info:
                Intent info = new Intent(this, AboutActivity.class);
                startActivity(info);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        active = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        active = true;
    }

    private void newNotice(String text){
        notice.setText(text);
        notice.setVisibility(View.VISIBLE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (active) {
                    notice.getHandler().post(new Runnable() {
                        @Override
                        public void run() {
                            notice.setVisibility(View.INVISIBLE);
                        }
                    });
                }
            }
        }).start();
    }

    /**
     * Takes the letter in the EditText guess and checks it with Hangman class.
     * Depending on the result from this a number of this happen.
     * 1. A notice message pop up
     * 2. the game is won/lost and the Result activity is started finishing this one.
     * 3. Sent on to a private class that updates the UI with the results from hangman.
     *
     * @param view - the onClick event from the user
     */
    public void guessButton(View view){
        char temp;
        try{
            temp = guess.getText().charAt(0);
            guess.setText("");
        }catch (Exception e){
            newNotice(getString(R.string.no_input_text));
            return;
        }
        if(hangman.hasUsedLetter(temp)){
            newNotice(getString(R.string.used_text));
            return;
        }
        if(!hangman.isLetter(temp)){
            newNotice(getString(R.string.not_letter_text));
            return;
        }

        hangman.guess(temp);

        if(hangman.hasWon()){
            Intent result = new Intent(view.getContext(), ResultActivity.class);
            result.putExtra("word", hangman.getRealWord());
            result.putExtra("hasWon", true);
            result.putExtra("triesLeft", hangman.getTriesLeft());
            startActivity(result);
            finish();
        }
        if(hangman.hasLost()){
            Intent result = new Intent(view.getContext(), ResultActivity.class);
            result.putExtra("word", hangman.getRealWord());
            startActivity(result);
            finish();
        }
        updateUI();
    }

    /**
     * Private class that updates these TextViews word, tries, guessed along with the ImageView hangImage
     */
    private void updateUI(){
        word.setText(hangman.getHiddenWord());
        tries.setText(hangman.getTriesLeft() + " " + getString(R.string.tries_text));
        setHangImage("hang"+hangman.getTriesLeft());
        guessed.setText(hangman.getBadLettersUsed());
    }

    private void setHangImage(String name){
        hangImage.setImageResource(getResources().getIdentifier(name, "drawable", getPackageName()));
    }

    private ArrayList<String> getAllWords(){
        ArrayList<String> allWords = new ArrayList<String>();
        allWords.add("CAT");
        allWords.add("SHEEP");
        allWords.add("TURTLE");
        allWords.add("DOG");
        allWords.add("BED");
        allWords.add("BOX");
        allWords.add("COMPUTER");
        allWords.add("DESK");
        allWords.add("PROGRAMMING");
        allWords.add("CAN");
        return allWords;
    }
}
