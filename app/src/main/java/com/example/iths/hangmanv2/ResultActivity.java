package com.example.iths.hangmanv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

/**
 * A class for the result activity.
 *
 * Created by Christian Heina on 15-11-06.
 */
public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Intent intent = getIntent();
        String word = intent.getStringExtra("word");
        boolean hasWon = intent.getBooleanExtra("hasWon", false);
        int triesLeft = intent.getIntExtra("triesLeft", 0);

        TextView result = (TextView) findViewById(R.id.game_end);
        TextView theWord = (TextView) findViewById(R.id.the_word);
        TextView triesLeftText = (TextView) findViewById(R.id.tries_left);

        if(hasWon){
            result.setText(getString(R.string.game_won));
        }else{
            result.setText(getString(R.string.game_lost));
        }
        theWord.setText(getString(R.string.the_word_text) + " " + word);
        triesLeftText.setText(getString(R.string.tries_left_text) + " " + triesLeft);

    }

    /**
     * Adds a menu to the action bar.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * when clicking the info icon or text in the actionbar this starts a new AboutActivity and finish() this one.
     * when clicking the play icon or text in the actionbar this starts a new PlayActivity and finish() this one.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.ic_play:
                Intent play = new Intent(this, PlayActivity.class);
                startActivity(play);
                finish();
                return true;
            case R.id.ic_info:
                Intent info = new Intent(this, AboutActivity.class);
                startActivity(info);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Starts a new MenuActivity and finish() this one.
     *
     * @param view - the onClick event from the user
     */
    public void resultBackButton(View view){
        Intent back = new Intent(this, MenuActivity.class);
        startActivity(back);
        finish();
    }
}
