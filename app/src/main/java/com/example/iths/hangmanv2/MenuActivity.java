package com.example.iths.hangmanv2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/**
 * A class for the menu activity.
 *
 * Created by Christian Heina on 15-11-06.
 */
public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    /**
     * Adds a menu to the action bar.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    /**
     * when clicking the info icon or text in the actionbar this starts a new AboutActivity.
     * when clicking the play icon or text in the actionbar this starts a new PlayActivity.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.ic_play:
                Intent play = new Intent(this, PlayActivity.class);
                startActivity(play);
                return true;
            case R.id.ic_info:
                Intent info = new Intent(this, AboutActivity.class);
                startActivity(info);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates a new PlayActivity.
     *
     * @param view - the onClick event from the user
     */
    public void playButton(View view){
        Intent play = new Intent(this, PlayActivity.class);
        startActivity(play);
    }

    /**
     * Creates a new AboutActivity.
     *
     * @param view - the onClick event from the user
     */
    public void aboutButton(View view){
        Intent about= new Intent(this, AboutActivity.class);
        startActivity(about);
    }

    /**
     * Finishes this activity and effectively the app as all other activities finishes when leaving them.
     *
     * @param view - the onClick event from the user
     */
    public void exitButton(View view){
        finish();
    }
}
